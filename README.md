# Jesternetes
Run Jest tests accross a Kubernetes cluster!

## Usage

```
Usage: j9scli [options] [command]

Options:
  -V, --version          output the version number
  -c, --config           Path to j9s config
  -d, --cwd <cwd>        set the current working directory for execution
  -h, --help             display help for command

Commands:
  build-image [options]
  push-image [options]
  create-job [options]
  delete-job [options]
  export-logs [options]
  set-sail [options]
  help [command]         display help for command
```

## Commands

### build-image [options]

### push-image [options]

### create-job [options]

### delete-job [options]

### export-logs [options]

### set-sail [options]
```
Usage: j9scli set-sail [options]

Builds the j9s image, pushes it to the registry, creates a j9s job, monitors for completion, and retrieves logs.

Options:
  -k, --kubeconfig <kubeconfig>  Kubeconfig to use, default to current context
  -n, --namespace <namespace>    Kubernetes namespace for created job (default: "default")
  -p, --path <path>              Path for source files (default: ".")
  -f, --file <file>              Location of j9s Dockerfile (default: "./Dockerfile.j9s")
  -t, --tag <tag>                Tag for image, may be of form name:tag
  -h, --help                     display help for command
```