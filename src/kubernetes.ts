import * as k8s from '@kubernetes/client-node';
import * as uuid from 'uuid';
import * as fs from 'fs';
import * as path from 'path';
import delay from 'delay';
import { ApiType, V1LabelSelector, V1JobStatus } from '@kubernetes/client-node';

export interface CreateJobOptions {
  kubeconfig: string,
  namespace: string,
  image: string
};

export interface DeleteJobOptions {
  kubeconfig: string,
  namespace: string
  jobId: string,
};

export interface GetJobStatusOptions {
  kubeconfig: string,
  namespace: string
  jobId: string,
};

export interface PollStatusOptions {
  kubeconfig: string,
  namespace: string
  jobId: string,
};

export interface ExportJobLogsOptions {
  kubeconfig: string,
  namespace: string
  jobId: string,
};

export interface J9sJob {
  jobId: string,
  k8sJobs: k8s.V1Job[]
};

export interface J9sJobStatus {
  startTime: Date,
  completionTime?: Date,
  active?: number,
  succeeded?: number,
  failed?: number
}

export interface J9sDeletionStatus {
  jobId: string,
  k8sStatuses: k8s.V1Status[]
};

const makeApiClient = <T extends ApiType>(kubeconfigPath: string, apiClientType: new (server: string) => T): T => {
  const kubeConfig = new k8s.KubeConfig();

  // Troubleshooting a doctl authentication problem: https://github.com/kubernetes-client/javascript/issues/496
  if (kubeconfigPath) {
    kubeConfig.loadFromFile(kubeconfigPath);
  } else {
    kubeConfig.loadFromDefault();
  }
  return kubeConfig.makeApiClient(apiClientType);
}

const makeJobName = (jobId: string) => `j9s-${jobId}`;
const makeLogDirectoryPath = (jobId: string) => path.join('.', '.j9s', jobId);
const makeLogFilePath = (jobId: string, podName: string) => path.join(makeLogDirectoryPath(jobId), `${podName}-log.json`);

export const createJob = async (jobOptions: CreateJobOptions): Promise<J9sJob> => {
  const k8sApi = makeApiClient(jobOptions.kubeconfig, k8s.BatchV1Api);
  const jobId = uuid.v4();
  const { body } = await k8sApi.createNamespacedJob(jobOptions.namespace, {
    kind: 'Job',
    metadata: {
      name: makeJobName(jobId),
      labels: {
        j9sId: jobId
      }
    },
    spec: {
      template: {
        spec: {
          containers: [{
            name: 'j9s',
            image: jobOptions.image
          }],
          restartPolicy: 'OnFailure'
        }
      },
      backoffLimit: 4
    }
  });

  return {
    jobId: jobId,
    k8sJobs: [body]
  };
};

export const deleteJob = async (jobOptions: DeleteJobOptions): Promise<J9sDeletionStatus> => {
  const k8sApi = makeApiClient(jobOptions.kubeconfig, k8s.BatchV1Api);
  const { body } = await k8sApi.deleteNamespacedJob(makeJobName(jobOptions.jobId), jobOptions.namespace);
  return {
    jobId: jobOptions.jobId,
    k8sStatuses: [body]
  };
};

export const getJobStatus = async (jobOptions: GetJobStatusOptions): Promise<J9sJobStatus> => {
  const k8sApi = makeApiClient(jobOptions.kubeconfig, k8s.BatchV1Api);
  const { body } = await k8sApi.readNamespacedJobStatus(makeJobName(jobOptions.jobId), jobOptions.namespace);

  if (!body.status) {
    throw Error('Status was not included in the response payload');
  }

  const status = body.status as V1JobStatus;

  return {
    startTime: status.startTime as Date,
    completionTime: status.completionTime,
    active: status.active,
    succeeded: status.succeeded,
    failed: status.failed
  }
};


export const pollStatus = (jobOptions: GetJobStatusOptions,
  onStatusCheck: (status: J9sJobStatus) => void,
  iterations: number = 10,
  timeoutMs: number = 10000): Promise<J9sJobStatus | undefined> => {
  return new Promise(async (resolve, reject) => {
    for (let i = 0; i < iterations; i++) {
      const status = await getJobStatus(jobOptions);

      onStatusCheck(status);

      if (status.completionTime !== undefined) {
        return resolve(status);
      }

      await delay(timeoutMs)
    }
    reject();
  });
};

export const exportJobLogs = async (jobOptions: ExportJobLogsOptions): Promise<void> => {
  const k8sApi = makeApiClient(jobOptions.kubeconfig, k8s.CoreV1Api);
  //  This interface sucks, would be nice to handle it as an object
  const { body } = await k8sApi.listNamespacedPod(jobOptions.namespace, undefined, undefined, undefined, undefined,
    `job-name=${makeJobName(jobOptions.jobId)}`);

  await createLogDirectory(jobOptions.jobId);

  body.items
    .filter(pod => !!(pod.metadata?.name))
    .map(async pod => {
      let podName = pod.metadata?.name as string;
      const { body } = await k8sApi.readNamespacedPodLog(podName, jobOptions.namespace);
      await fs.promises.writeFile(makeLogFilePath(jobOptions.jobId, podName), JSON.stringify(body, null, 2));
    });
}

const createLogDirectory = async (jobId: string): Promise<void> => {
  try {
    await fs.promises.mkdir(makeLogDirectoryPath(jobId), { recursive: true });
  } catch (error) {
    throw Error(`Error creating temporary folder for jobs logs: ${error.message}`);
  }
}
