#!/usr/bin/env node

import program from 'commander';

import {
  buildImageCommand,
  pushImageCommand,
  createJobCommand,
  deleteJobCommand,
  exportJobLogsCommand,
  sailCommand
} from './commands';

type CommandType = (...args: any) => Promise<any>;
const execCommand = (commandFunc: CommandType) =>
  (command: any) => {
    if (command.parent.cwd) {
      process.chdir(command.parent.cwd);
    }

    return commandFunc(command) as Promise<void>;
  }

const main = async (argv: string[]) => {
  program
    .name('j9scli')
    .version('1.0.0')
    .option('-c, --config', 'Path to j9s config', 'j9s.config')
    .option('-d, --cwd <cwd>', 'set the current working directory for execution');

  program
    .command('build-image')
    .option('-p, --path <path>', 'Path for source files', '.')
    .option('-f, --file <file>', 'Location of j9s Dockerfile', './Dockerfile.j9s')
    .option('-t, --tag <tag>', 'Tag for image, may be of form name:version')
    .action(execCommand(buildImageCommand));

  program
    .command('push-image')
    .option('-i, --image <image>', 'Image to push (requires registry to be configured)')
    .action(execCommand(pushImageCommand));

  program
    .command('create-job')
    .option('-k, --kubeconfig <kubeconfig>', 'Kubeconfig to use, default to current context', '~/.kube/config')
    .option('-n, --namespace <namespace>', 'Kubernetes namespace for created job', 'default')
    .option('-i, --image <image>', 'The j9s test image')
    .action(execCommand(createJobCommand));

  program
    .command('delete-job')
    .option('-k, --kubeconfig <kubeconfig>', 'Kubeconfig to use, default to current context', '~/.kube/config')
    .option('-n, --namespace <namespace>', 'Kubernetes namespace for created job', 'default')
    .option('-j, --jobId <jobId>', 'The j9s job id')
    .action(execCommand(deleteJobCommand));

  program
    .command('export-logs')
    .option('-k, --kubeconfig <kubeconfig>', 'Kubeconfig to use, default to current context', '~/.kube/config')
    .option('-n, --namespace <namespace>', 'Kubernetes namespace for created job', 'default')
    .option('-j, --jobId <jobId>', 'The j9s job id')
    .action(execCommand(exportJobLogsCommand));

  program
    .command('set-sail')
    .description('Builds the j9s image, pushes it to the registry, creates a j9s job, monitors for completion, and retrieves logs.')
    .option('-k, --kubeconfig <kubeconfig>', 'Kubeconfig to use, default to current context')
    .option('-n, --namespace <namespace>', 'Kubernetes namespace for created job', 'default')
    .option('-p, --path <path>', 'Path for source files', '.')
    .option('-f, --file <file>', 'Location of j9s Dockerfile', './Dockerfile.j9s')
    .option('-t, --tag <tag>', 'Tag for image, may be of form name:tag')
    .action(execCommand(sailCommand));

  program
    .parse(argv);
};

if (require.main === module) {
  main(process.argv).catch((error) => {
    console.error(error);
  });
}

export { }