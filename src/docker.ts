import execa from 'execa';

export interface BuildImageOptions {
  path: string,
  tag: string,
  file: string
}

export interface PushImageOptions {
  image: string
}

export interface DockerExecResult {
  stderr?: string,
  stdout?: string
}

export interface BuildImageResult extends DockerExecResult { }

export interface PushImageResult extends DockerExecResult { }

export const buildImage = async (options: BuildImageOptions): Promise<BuildImageResult> => {
  const cmdArgs = ['build'];

  // Optional file option
  if (options.file) {
    cmdArgs.push('--file', options.file);
  }

  // Optional tag option
  if (options.tag) {
    cmdArgs.push('--tag', options.tag);
  }

  // Handle the path, this is required
  if (options.path) {
    cmdArgs.push(options.path ? options.path : '.')
  }

  return dockerExec(cmdArgs);
};

export const pushImage = async (options: PushImageOptions): Promise<PushImageResult> => {
  const cmdArgs = ['push', options.image];
  return dockerExec(cmdArgs);
};

export const dockerExec = (cmdArgs: string[]): Promise<DockerExecResult> => {
  return new Promise<DockerExecResult>(async (resolve, reject) => {
    try {
      // execa is really cool! https://github.com/sindresorhus/execa
      // I learned about it through the official kubernetes node client repository while
      // troubleshooting a doctl authentication problem: https://github.com/kubernetes-client/javascript/issues/496
      const execReturn = await execa('docker', cmdArgs);
      const { stdout, stderr } = execReturn;
      resolve({ stdout, stderr });
    } catch ({ stdout, stderr, command }) {
      console.error(`ERROR: unable to execute command '${command}'.`);
      reject({ stdout, stderr });
    }
  });
}