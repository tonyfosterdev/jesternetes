import {
  buildImage, BuildImageOptions,
  pushImage, PushImageOptions
} from './docker';

import {
  createJob, CreateJobOptions,
  deleteJob, DeleteJobOptions,
  exportJobLogs, ExportJobLogsOptions,
  pollStatus, PollStatusOptions, J9sJobStatus
} from './kubernetes';

// Seperate interfaces allows for decoupling the underlying services from the cli.
export interface BuildImageCommandOptions extends BuildImageOptions { }
export interface PushImageCommandOptions extends PushImageOptions { }
export interface CreateJobCommandOptions extends CreateJobOptions { }
export interface DeleteJobCommandOptions extends DeleteJobOptions { }
export interface ExportJobLogsCommandOptions extends ExportJobLogsOptions { }
export interface PollStatusCommandOptions extends PollStatusOptions { }

export interface SailCommandOptions {
  kubeconfig: string,
  namespace: string
  path: string,
  tag: string,
  file: string
}

export const buildImageCommand = async (options: BuildImageCommandOptions) => {
  const command = 'build-image';
  try {
    printLog(command, 'Building image...');
    const result = await buildImage(options);
    printLog(command, 'Done!');
    return result;
  } catch (error) {
    printError(command, `Unable to build image: ${error.stderr}`);
    exitWithError();
  }
};

export const pushImageCommand = async (options: PushImageCommandOptions) => {
  const command = 'push-image';
  try {
    printLog(command, 'Pushing image to registry...');
    const result = await pushImage(options);
    printLog(command, 'Done!');
    return result;
  } catch (error) {
    printError(command, `Unable to push image: ${error.stderr}`);
    exitWithError();
  }
};

export const createJobCommand = async (options: CreateJobCommandOptions) => {
  const command = 'create-job';
  try {
    printLog(command, 'Creating jesternetes job on kubernetes cluster...');
    const result = await createJob(options);
    printLog(command, `Success! Jesternetes job created with id: ${result.jobId} `);
    return result;
  } catch (error) {
    printError(command, `Unable to create job: ${error}`);
    exitWithError();
  }
}

export const deleteJobCommand = async (options: DeleteJobCommandOptions) => {
  const command = 'delete-job';
  try {
    printLog(command, 'Deleting jesternetes job information from kubernetes cluster...');
    const result = await deleteJob(options);
    printLog(command, `Success! Scheduled deletion for job ${result.jobId}`);
  } catch (error) {
    printError(command, `Unable to delete job: ${error.stderr}`);
    exitWithError();
  }
}

export const exportJobLogsCommand = async (options: ExportJobLogsCommandOptions) => {
  const command = 'export-logs';
  try {
    printLog(command, 'Getting logs for jesternetes job...');
    const result = await exportJobLogs(options);
    printLog(command, `Success! Logs have been saved.`);
  } catch (error) {
    printError(command, `Unable to export job logs: ${error.stderr}`);
    exitWithError();
  }
}

export const pollStatusCommand = async (options: PollStatusCommandOptions) => {
  const command = 'poll-status';
  const { kubeconfig, namespace, jobId } = options;
  try {
    const finalStatus = await pollStatus({ kubeconfig, namespace, jobId }, (status) => {
      printLog(command, `(job ${jobId} status) active: ${status.active || 0}, succeeded: ${status.succeeded || 0}, failed: ${status.failed || 0}`);
    }) as J9sJobStatus; // Rejection with undefined is throwing TS for a loop

    if (finalStatus.completionTime && !finalStatus.failed) {
      printLog(command, `job ${jobId} completed successfully!`)
    } else {
      printLog(command, `job ${jobId} failed!`)
    }
  } catch (error) {
    printError(command, `Job didn't complete in a timely manner: ${error.stderr}`);
    exitWithError();
  }
}

export const sailCommand = async (options: SailCommandOptions) => {
  const command = 'set-sail';
  const buildImageResult = await buildImageCommand({
    path: options.path,
    tag: options.tag,
    file: options.file
  });

  const image = options.tag;
  const { kubeconfig, namespace } = options;
  const pushImageResult = await pushImageCommand({ image });
  const createJobResult = await createJobCommand({
    kubeconfig,
    namespace,
    image
  });

  const jobId = createJobResult?.jobId as string;
  await pollStatusCommand({ kubeconfig, namespace, jobId })
  await exportJobLogsCommand({ kubeconfig, namespace, jobId });
};

export const exitWithError = (code: number = 1) => {
  console.error(`Exiting with error code ${code}`);
  process.exit(code);
}

const printLog = (command: string, message: string) => console.log(`[${command}] ${message}`);
const printError = (command: string, message: string) => console.error(`[${command}] ERROR ${message}`);
