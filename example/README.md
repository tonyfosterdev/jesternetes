# Jesternetes Example

## Running Example

Build Jesternetes from the project root:

```
~/jesternetes$ npm install
~/jesternetes$ npm run build
```
Install the dependencies for the example:

```
~/jesternetes/example$ npm install
```
Note: You'll need to complete the pre-requisites (TODO: Link to guide for setting up docker and k8s) and modify the `--tag` option on the `j9s:set-sail` script.


And set sail...
```
~/jesternetes/example$ npm run j9s:set-sail
> example-app@1.0.0 j9s:set-sail /home/tony/repos/jesternetes/example
> j9scli set-sail --tag registry.gitlab.com/tonyfosterdev/jesternetes/j9s-testapp

[build-image] Building image...
[build-image] Done!
[push-image] Pushing image to registry...
[push-image] Done!
[create-job] Creating jesternetes job on kubernetes cluster...
[create-job] Success! Jesternetes job created with id: c0481e7b-8f03-4ced-a879-fd9556b8630e 
[poll-status] (job c0481e7b-8f03-4ced-a879-fd9556b8630e status) active: 1, succeeded: 0, failed: 0
[poll-status] (job c0481e7b-8f03-4ced-a879-fd9556b8630e status) active: 1, succeeded: 0, failed: 0
[poll-status] (job c0481e7b-8f03-4ced-a879-fd9556b8630e status) active: 0, succeeded: 1, failed: 0
[poll-status] job c0481e7b-8f03-4ced-a879-fd9556b8630e completed successfully!
[export-logs] Getting logs for jesternetes job...
[export-logs] Success! Logs have been saved.
```

Your logs should appear under the `/jesternestes/example/.j9s/[JOB ID]` folder.