const delay = require('delay');

const doDelay = (delayMs, str) => delay(delayMs).then(() => str);

module.exports = doDelay;
