const doDelay = require('./component1');

const delayTest = (delayMs, str) => 
    it(`should delay for ${delayMs}ms and then return ${str}`, (done) => {
        return doDelay(delayMs, str).then(s => {
            expect(s).toEqual(str);
            done();
        });
    });

const getTestCount = () => 10;
const getTestDelayMs = () => 1;

const runTests = () => {
    for (let i = 0; i < getTestCount(); i++) {
        delayTest(getTestDelayMs(), `Test #${i + 1}`);
    }
};

describe('testSuite1::doDelay', () => {
    runTests();
});

describe('testSuite2::doDelay', () => {
    runTests();
});

describe('testSuite3::doDelay', () => {
    runTests();
});

